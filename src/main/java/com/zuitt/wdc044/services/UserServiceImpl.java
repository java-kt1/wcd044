package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    //Dependency Injection
    //@Autowired means an instance of a class that implements
    //UserRepository will be created in our container.
    @Autowired
    private UserRepository userRepository;

    public void createUser(User user) {
        //Save the user in our table
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        //Null can be returned as a values if the user record cannot be found.
        return Optional.ofNullable(userRepository.findByUsername(username));
    }


}
